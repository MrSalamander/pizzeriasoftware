package Controller;

public class Meal{
    private int Id;
    private String Name;
    private float[] Price;
    private String Ingredients;
    public Meal(int Id, String Name, float[] Price, String Ingredients)
    {
        this.Id = Id;
        this.Name = Name;
        this.Ingredients = Ingredients;
        this.Price = Price;
    }
    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public float[] getPrice() {
        return Price;
    }

    public void setPrice(float[] Price) {
        this.Price = Price;
    }

    public String getIngredients() {
        return Ingredients;
    }

    public void setIngredients(String Ingredients) {
        this.Ingredients = Ingredients;
    }

    
    public int getId(){
        return this.Id;
    }
    
    

}