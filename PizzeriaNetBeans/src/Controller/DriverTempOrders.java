/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

/**
 *
 * @author Aleksander
 */
public class DriverTempOrders {
    private int[] Orders_id;
    private String name;
    public DriverTempOrders(String name,int[] ids)
    {
        this.name = name;
        Orders_id = ids;
    }

    public int[] getOrders_id() {
        return Orders_id;
    }

    public void setOrders_id(int[] Orders_id) {
        this.Orders_id = Orders_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
}
