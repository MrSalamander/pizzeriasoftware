
package Controller;

public class Driver {
    private String login;
    private String password;
    private int status;
    public Driver()
    {
        
    }
    public Driver(String login,String password)
    {
    this.login = login;
    this.password = password;
    status = 0;
    }
    public void setStatus(int status)
    {
        this.status = status;
    }
    public int getStatus() {
        return status;
    }
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
}
