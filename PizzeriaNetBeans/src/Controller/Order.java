package Controller;


//import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;


/**
 *
 * @author Tomasz
 * 
 **/


public class Order implements Comparable<Order>{
    
    private int orderID;
    private String date;
    private String address;
    private String city;
    private String tel;
    private String comments;
    private int type;
    private int status;
    private String hour;
    private ArrayList<Order_Meal> orderedMeals = new ArrayList<>();

    public Order()
    {}
    public Order(String address, String city, String tel, String comments, int type, int status, String hour, ArrayList<Order_Meal> orderedMeals)
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        Date date = new Date();
        this.date = dateFormat.format(date);
        this.address = address;
        this.city = city;
        this.tel = tel;
        this.comments = comments;
        this.type = type;
        this.status = status;
        this.hour = hour;
        this.orderedMeals = new ArrayList(orderedMeals);
    }
    public Order(int orderID, String date, String address, String city, String tel, String comments, int type, int status, String hour, ArrayList<Order_Meal> orderedMeals)
    {
        this.orderID = orderID;
        this.date = date;
        this.address = address;
        this.city = city;
        this.tel = tel;
        this.comments = comments;
        this.type = type;
        this.status = status;
        this.hour = hour;
        this.orderedMeals = new ArrayList(orderedMeals);
    }
    public Order(int orderID, Date date, String address, String city, String tel, String comments, int type, int status, Date hour, ArrayList<Order_Meal> orderedMeals)
    {
        this.orderID = orderID;
        this.date = date.toString();
        this.address = address;
        this.city = city;
        this.tel = tel;
        this.comments = comments;
        this.type = type;
        this.status = status;
        this.hour = hour.toString();
        this.orderedMeals = new ArrayList(orderedMeals);
    }
    @Override
    public int compareTo(Order order) {
    return this.hour.compareTo(order.getHour());
  }
    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public ArrayList<Order_Meal> getOrderedMeals() {
        return orderedMeals;
    }

    public void setOrderedMeals(ArrayList<Order_Meal> orderedMeals) {
        this.orderedMeals = new ArrayList(orderedMeals);
    }

    public static Order getFromListByID(List<Order> list, int Id)
    {
        Order temp =null;
        for(Order order : list)
        {
          if(order.getOrderID() == Id) temp = order; 
        }
        return temp;
    }
  
    
}
