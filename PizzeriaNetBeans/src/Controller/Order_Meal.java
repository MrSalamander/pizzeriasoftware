/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

/**
 *
 * @author Tomasz
 */
public class Order_Meal {
    
    private int OrderID;
    private int MealID;
    private String Ingredients;
    private int Size;
 
    public Order_Meal()
    {

    }
    public Order_Meal(int OrderID, int MealID, String Ingredients, int Size)
    {
        this.OrderID = OrderID;
        this.MealID = MealID;
        this.Ingredients = Ingredients;
        this.Size = Size;
    }

    public int getMealID() {
        return MealID;
    }

    public void setMealID(int MealID) {
        this.MealID = MealID;
    }

    public String getIngredients() {
        return Ingredients;
    }

    public void setIngredients(String Ingredients) {
        this.Ingredients = Ingredients;
    }

    public int getSize() {
        return Size;
    }

    public void setSize(int Size) {
        this.Size = Size;
    }
    
}
