package Controller;

import java.util.ArrayList;
import java.util.List;

// Method return List of Names for every meal in database.
// Id in list is exacly id of object.
public abstract class ExtraMethods {
    public static List<String> getListOfMealNames(List<Meal> meals)
    {
        List<String> list = new ArrayList<>();
        meals.forEach(item->list.add(item.getName()));
        return list;
    }
    
   
}
