package Controller;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Queries {
    private Connection con;
    private Statement stmt;
   
    //Constructor witch declaration database connection.
    public Queries(){
    this.SetConnectionToDB(); //initialize Connection and (inside) Statement stmt
    System.out.println("queries constructor done");
    }
    
    // Method used to check if login and pass match to any cell in DB
    // The NEW one method
    public int Login(String login, String pass) throws SQLException {
        int a=0;
        try
        {
            Class.forName("com.mysql.cj.jdbc.Driver");
            ResultSet rs =  stmt.executeQuery("select * from Users where login = '" + login + "' and pass = '" + pass + "' ");
            while(rs.next()) {
                    a= rs.getInt(4);
            }rs.close();
        } catch (ClassNotFoundException e)
        {
            System.out.println("BĹ‚Ä…d! " + e.getMessage());
        }
        return a;
    }
    
    // Method used to check if login and pass match to any cell in DB
    public static int login(String login, String pass) throws SQLException {
        int a=0;
        try
        {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://mysql-805883.vipserv.org:3306/tom112_projektKF?useTimezone=true&serverTimezone=UTC","tom112","projektKF");
            Statement stmt = con.createStatement();
            ResultSet rs =  stmt.executeQuery("select * from Users where login = '" + login + "' and pass = '" + pass + "' ");
            while(rs.next()) {
                    a= rs.getInt(4);
            }rs.close();
        } catch (ClassNotFoundException e)
        {
            System.out.println("BĹ‚Ä…d! " + e.getMessage());
        }
        return a;
    }
    
    
    
    //The new one method, for object (non-static)
    public void SetConnectionToDB(){
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            this.con = DriverManager.getConnection("jdbc:mysql://mysql-805883.vipserv.org:3306/tom112_projektKF?useTimezone=true&serverTimezone=UTC", "tom112", "projektKF");
            if(con != null ) System.out.println("stmt nie jest null"); 
            this.stmt = con.createStatement(); //initialize statement 
            if(stmt != null ) System.out.println("stmt nie jest null");  
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println(e.getLocalizedMessage() + " (new wersion error)");
        }
    }
    
    //The old one method 
    public static Connection setConnectionToDB() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://mysql-805883.vipserv.org:3306/tom112_projektKF?useTimezone=true&serverTimezone=UTC", "tom112", "projektKF");
            return con;
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println(e.getLocalizedMessage() + "podaje dalej null, czyli program siÄ™ wykrzaczy");
            return null;
        }
    }
    
   
    
    //The old one method (USELESS method !!!!!)
    public static Statement getFromDB(Connection con)
    {

        Statement stmt = null;
        try{
            stmt = con.createStatement();
        } catch (SQLException e)
        {
            System.out.println("BĹ‚Ä…d! " + e.getMessage());
        }
        return stmt;
    }
    
    
    // This method returns all orders from DB
    // The NEW one method 
    public List<Order> GetOrdersToList() throws SQLException {
        int id, type, status;
        String date, address, city, tel, comments, hour;
        ArrayList<Order_Meal> mealTmp = new ArrayList<>();
        List<Order> orderList = new ArrayList<>();
        Order tempOrder;
        Time time1;
        String query = "select * from Orders";
            try{
                Statement stmt2 = con.createStatement();
                Statement stmt3 = con.createStatement();
                ResultSet rs = stmt2.executeQuery(query); // <---- NullPointerException
                ResultSet rs2;
                if(rs == null)System.out.println("rs = null");
                while(rs.next())
                {
                    id = rs.getInt(1);
                    date = rs.getDate(2).toString();
                    address = rs.getString(3);
                    city = rs.getString(4);
                    tel = rs.getString(5);
                    comments = rs.getString(6);
                    type = rs.getInt(7);
                    status = rs.getInt(8);
                    time1 = rs.getTime(9);
                    time1.setHours(time1.getHours()-1);
                    //hour = rs.getTime(9).toString();
                      hour = time1.toString();
                      
                    query = "select * from Orders_Meals where orderID = '" + id + "' ";
                    rs2 = stmt3.executeQuery(query);
                    mealTmp.clear();
                    while(rs2.next())
                    {
                        mealTmp.add(new Order_Meal(rs2.getInt(1), rs2.getInt(2), rs2.getString(3), rs2.getInt(4)));
                        //System.out.print("dodaję: "+rs2.getString(3)+" ");
                    }        
                    tempOrder = new Order(id, date, address, city, tel, comments, type, status, hour, mealTmp);
                    orderList.add(tempOrder); //finish, add order to list.
                    if(mealTmp.size()>0)System.out.println("id zamowienia: " + id + " dań: "+mealTmp.size());  
                    
                }
                rs.close();
                stmt2.close();
                return orderList;
            }catch(Exception e){System.out.print(e.getLocalizedMessage());e.printStackTrace();return null;}
            
    }
    
    
    // Method used to get an order with specific status or next method - two statuses
    // Additional info: only used by cook
    
    public static List<Order> getSpecStatus(List<Order> orders, int status)
    {
        List<Order> newOrders = new ArrayList<>();
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd HH:mm:ss");
        int i = 0;
        Date date = new Date();
        String dataWString2 = ft.format(date);
        String dataWString = orders.get(i).getHour();
        int dzien = (10 * Integer.parseInt(String.valueOf(dataWString2.charAt(8)))) + Integer.parseInt(String.valueOf(dataWString2.charAt(9)));
        int miesiac = (10 * Integer.parseInt(String.valueOf(dataWString2.charAt(5)))) + Integer.parseInt(String.valueOf(dataWString2.charAt(6)));
        int rok = (1000 * Integer.parseInt(String.valueOf(dataWString2.charAt(0)))) + (100 * Integer.parseInt(String.valueOf(dataWString2.charAt(1)))) + (10 * Integer.parseInt(String.valueOf(dataWString2.charAt(2)))) + Integer.parseInt(String.valueOf(dataWString2.charAt(3)));
        int godzina = (10 * Integer.parseInt(String.valueOf(dataWString.charAt(0)))) + Integer.parseInt(String.valueOf(dataWString.charAt(1)));
        int minuta = (10 * Integer.parseInt(String.valueOf(dataWString.charAt(3)))) + Integer.parseInt(String.valueOf(dataWString.charAt(4)));
        Date date2 = new Date(rok-1900, miesiac-1, dzien-10, godzina, minuta, 0);
        Date currentDatePlusHalf =  new Date(date.getTime() + 30*60*1000);
        
        while(i < orders.size())
        {
            dataWString = orders.get(i).getHour();
            dzien = (10 * Integer.parseInt(String.valueOf(dataWString2.charAt(8)))) + Integer.parseInt(String.valueOf(dataWString2.charAt(9)));
            miesiac = (10 * Integer.parseInt(String.valueOf(dataWString2.charAt(5)))) + Integer.parseInt(String.valueOf(dataWString2.charAt(6)));
            rok = (1000 * Integer.parseInt(String.valueOf(dataWString2.charAt(0)))) + (100 * Integer.parseInt(String.valueOf(dataWString2.charAt(1)))) + (10 * Integer.parseInt(String.valueOf(dataWString2.charAt(2)))) + Integer.parseInt(String.valueOf(dataWString2.charAt(3)));
            godzina = (10 * Integer.parseInt(String.valueOf(dataWString.charAt(0)))) + Integer.parseInt(String.valueOf(dataWString.charAt(1)));
            minuta = (10 * Integer.parseInt(String.valueOf(dataWString.charAt(3)))) + Integer.parseInt(String.valueOf(dataWString.charAt(4)));
            date2 = new Date(rok-1900, miesiac-1, dzien-10, godzina-1, minuta, 0);
            if(currentDatePlusHalf.compareTo(date2)>=0)
            {
                newOrders.add(orders.get(i));
            }
            i++;
        }
        return newOrders;
    }
    
    public static List<Order> getSpecStatus(List<Order> orders, int status, int status2)
    {
        List<Order> newOrders = new ArrayList<>();
        int i = 0;
        while(i < orders.size())
        {
            if((orders.get(i).getStatus()==status || orders.get(i).getStatus()==status2))
            {
                newOrders.add(orders.get(i));
            }
            i++;
        }
        return newOrders;
    }
    public static List<Order> getSpecStatus(List<Order> orders, int status, int status2,int status3)
    {
        if(orders != null){
        List<Order> newOrders = new ArrayList<>();
        int i = 0;
        while(i < orders.size())
        {
            if((orders.get(i).getStatus()==status || orders.get(i).getStatus()==status2 || orders.get(i).getStatus()==status3))
            {
                newOrders.add(orders.get(i));
            }
            i++;
        }
        return newOrders;
        }else{System.out.println("OrderList is null");return null;}   
    }

    // Method that adds obiect newOrder to DB
    // The NEW one method 
    public void AddOrder(Order newOrder)
    {
        int orderID = Queries.maximumID("Orders");
        String st2 = " ";
        try{
            String queryPartOne = "INSERT INTO Orders (orderID, date, address, city, tel, comments, type, status, hour) VALUES";
            String query = (queryPartOne + " ('" + orderID + "', '" + newOrder.getDate() + "', '" + newOrder.getAddress() + "', '" + newOrder.getCity() + "', '"+ newOrder.getTel() + "', '" + newOrder.getComments() + "', '" + newOrder.getType() + "', '" + newOrder.getStatus() + "', '" + newOrder.getHour() + "')");
            int update =  stmt.executeUpdate(query);
            int i=0;
            stmt.clearBatch();
            while( i < newOrder.getOrderedMeals().size())
            {
                System.out.println("order id max = " + orderID);
                //Tomaszu proszę tutaj popatrzeć na to czemu jest coś nie tak z ID
                update =  stmt.executeUpdate("INSERT INTO Orders_Meals (orderID, mealID, ingredients, Rozmiar) VALUES(" + orderID + ", " + newOrder.getOrderedMeals().get(i).getMealID() + ", '" + newOrder.getOrderedMeals().get(i).getIngredients() + "', " + newOrder.getOrderedMeals().get(i).getSize() + ")");
                //update =  stmt.executeUpdate("INSERT INTO Orders_Meals (orderID, mealID, ingredients, Rozmiar) VALUES ('1', '" + '2' + "', " + '3' + ", " + 4 + ")");
               
                i++;
            }
            //ResultSet st1 = stmt.executeQuery("desc Orders_Meals");
            //st2 = st1.getString(0);
            //st1.close();
        }catch(Exception e){
            //System.out.println(st2);
            System.out.println("adding new order Failed!");
            e.printStackTrace();
        }
    }
    
    // Method that adds obiect newOrder to DB
    // The old one method 
    /*public static void addOrder(Order newOrder)
    {
        try
        {
            int orderID = Queries.maximumID("Orders");
            Connection con = Queries.setConnectionToDB();
            try (Statement stmt = Queries.getFromDB(con)) {
                String pierwszaCzesc = "INSERT INTO Orders (orderID, date, address, city, tel, comments, type, status, hour) VALUES";
                String sql = (pierwszaCzesc + " ('" + orderID + "', '" + newOrder.getDate() + "', '" + newOrder.getAddress() + "', '" + newOrder.getCity() + "', '"+ newOrder.getTel() + "', '" + newOrder.getComments() + "', '" + newOrder.getType() + "', '" + newOrder.getStatus() + "', '" + newOrder.getHour() + "')");
                int update =  stmt.executeUpdate(sql);
                stmt.close();
                int i=0;
                while( i < newOrder.getOrderedMeals().size())
                {
                    update =  stmt.executeUpdate("INSERT INTO Orders_Meals (orderID, mealID, ingredients, Rozmiar) VALUES ('" + orderID + "', '" + newOrder.getOrderedMeals().get(i).getMealID() + "', " + newOrder.getOrderedMeals().get(i).getIngredients() + "', " + newOrder.getOrderedMeals().get(i).getSize() + "')");
                    i++;
                }
            }
            con.close();
        } catch (SQLException e) {
        }
    }*/

    // The NEW version of method
    public void ChangeStatus(int status, int OrderID)
    {
        try
        {
            int update = stmt.executeUpdate("Update Orders Set status =" + status + " Where OrderID = " + OrderID + " ");
        }catch (SQLException e) {System.out.println("ChangeStatus Failed!"); e.printStackTrace();}
    }
    // The OLD version of method
    public static void changeStatus(int status, int OrderID)
    {
        try
        {
            Connection con = Queries.setConnectionToDB();
            Statement stmt = Queries.getFromDB(con);

            int update = stmt.executeUpdate("Update Orders Set status =" + status + " Where OrderID = " + OrderID + " ");

        }catch (SQLException e) {
        }
    }


    // Method used to get list of Meals (Menu) 
    
    public static List<Meal> getMeals() throws SQLException
    {
        List<Meal> mealList = new ArrayList<>();

        Connection con = Queries.setConnectionToDB();
        Statement stmt = Queries.getFromDB(con);

        String sql = "select * from Meals";
        ResultSet rs =  stmt.executeQuery(sql);
        String ingredients;
        int i=0;
        float[] priceList = new float[4];
        String price[];
        while(rs.next())
        {
            price = rs.getString(3).split("/");
            for(i=0; i<price.length; i++)
            {
                priceList[i] = Float.parseFloat(price[i]);
            }
            mealList.add(new Meal(rs.getInt(1), rs.getString(2), priceList, ""));
        }
        //rs.close();
        while(i<mealList.size())
        {
            //cSystem.out.println(i);
            sql = "select * from Meals_Ingredients where '" + mealList.get(i).getId() + "' ";
            rs = stmt.executeQuery(sql);

            while(rs.next()) {
                mealList.get(i).setIngredients(mealList.get(i).getIngredients() + rs.getString(2));
            }
            i++;
        }rs.close();
        return mealList;
    }
    
    
    // Method that returns maximum index of all datas in DB
    
    public static int maximumID(String table)
    {
        int naj=0;
        try
        {
            Connection con = Queries.setConnectionToDB();
            ResultSet rs;
            try (Statement stmt = Queries.getFromDB(con)) {
                rs = stmt.executeQuery("select * from " + table + " " );
                int tmp=0;
                while(rs.next()){
                    if((tmp = rs.getInt(1)) > naj) naj = tmp + 1;
                    //System.out.println(tmp);
                }
            }
            rs.close();
        }catch (SQLException e) {
        }
        return naj + 1;
    }
    
    public String getIngredientName(int recipeID)
    {
        String name = "";   
        String query = "select recipeID, name from Meals where recipeID = " + recipeID;
            try{
                Statement stmt2 = con.createStatement();
                Statement stmt3 = con.createStatement();
                ResultSet rs = stmt2.executeQuery(query); // <---- NullPointerException
                ResultSet rs2;
                if(rs == null)System.out.println("rs = null");
                while(rs.next())
                {
                    name = rs.getString(2);
                }
                rs.close();
                stmt2.close();
            }catch(SQLException e){System.out.print(e.getLocalizedMessage());return null;}
        return name;
    } 
    
}
