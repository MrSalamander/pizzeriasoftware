/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.Panels;

import Controller.Order;
import Controller.Queries;
import UserInterface.MainFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Aleksander
 */
public class MainCookPanel extends javax.swing.JPanel {

    /**
     * Creates new form MainCookPanel
     * 
     */
    Timer timer2;
    List<Order> orderList;
    String[][] table;
    private int selectedRow;
            
    public MainCookPanel(){
        
        
        timer2 = new Timer(1000,TableFillListener);
        timer2.setDelay(60000);
        timer2.start();
        initComponents();
        
        
        jTable2.getColumnModel().getColumn(0).setMaxWidth(50);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        Clock = new javax.swing.JTextField();

        jButton1.setText("Zrobione!");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jTable2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "ID", "Nazwa", "Składniki", "Godzina"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable2);

        Clock.setEditable(false);
        Clock.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 600, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 49, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(Clock)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 241, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Clock, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(156, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        //DefaultTableModel model = (DefaultTableModel)jTable2.getModel();
        try
        {
            selectedRow = jTable2.getSelectedRow();
        int ID = 0;
        ID = Integer.parseInt(jTable2.getValueAt(selectedRow,0).toString());
        
        MainFrame.queriesObj.ChangeStatus(2, ID);
        }
        catch(NullPointerException | java.lang.ArrayIndexOutOfBoundsException e)
        {
            
        }
        
    }//GEN-LAST:event_jButton1ActionPerformed

    
    
    ActionListener TimerListener = new ActionListener()
        {
        @Override
        public void actionPerformed(ActionEvent e)
            {
            System.out.println("timer 1");
            Date date = new Date();
            DateFormat timeFormat = new SimpleDateFormat("HH:mm"); 
             String time = timeFormat.format(date);
             Clock.setText(time);
            };
        };
    
    ActionListener TableFillListener = new ActionListener()
            {
                
            public void actionPerformed(ActionEvent e){
               if(MainFrame.OrderList!=null)
                 {
                     
                     orderList = Queries.getSpecStatus(MainFrame.OrderList, 0, 1);
                     Collections.sort(orderList);
                     //orderList = Queries.getSpecStatus(MainFrame.OrderList, 1);
                     DefaultTableModel model = (DefaultTableModel) jTable2.getModel();
                     model.setRowCount(0);
                     int i = 0;
                     Order order;
                     int id, status;
                     String ingredients, hour, name;
                     while(i < orderList.size())
                     {
                        order = orderList.get(i);
                        id = order.getOrderID();
                        hour = order.getHour();
                        status = order.getStatus();
                        ingredients = "";
                        name = "";
                        for(int j = 0; j < order.getOrderedMeals().size(); j++)
                        {
                            name = MainFrame.queriesObj.getIngredientName(order.getOrderedMeals().get(j).getMealID()) + "\n ";
                            ingredients += order.getOrderedMeals().get(j).getIngredients() + "\n ";
                        }
                        model.addRow(new Object[]{id,name,ingredients,hour});
                        i++;
                     }
                 }
                };
            };
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField Clock;
    private javax.swing.JButton jButton1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable2;
    // End of variables declaration//GEN-END:variables
}
