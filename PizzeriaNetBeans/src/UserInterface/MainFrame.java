package UserInterface;
import Controller.Order;
import Controller.Queries;
import java.sql.SQLException;
import java.util.List;
import java.util.TimerTask;
import java.util.Timer;

public class MainFrame extends javax.swing.JFrame {
    public int LoginStatus = 0;
    Timer timer1;
    public static List<Order> OrderList;
    public static Queries queriesObj;
    public MainFrame(int LoginStatus) {
        queriesObj = new Queries();
        this.LoginStatus = LoginStatus;
        this.setTitle("Pizzeria FedikoSollini & Kamińskonacci");
        initComponents();
        this.setVisible(true);
        // take list of orders every minute from db.
        spawnTabPanels();
        
        
        
    }

    private void spawnTabPanels()
    {
        switch(LoginStatus)
        {
            case 2:
                Tab.remove(mainBossPanel1);
                Tab.remove(mainWaitressPanel2);
                Tab.remove(mainDeliveryManPanel2);
            case 3:
                Tab.remove(mainBossPanel1);
                Tab.remove(mainDeliveryManPanel2);
                Tab.remove(mainCookPanel2);
            case 4:
                Tab.remove(mainBossPanel1);
                Tab.remove(mainWaitressPanel2);
                Tab.remove(mainCookPanel2);
        }
    }
        static public void refreshTable()
        {
            try{
            OrderList = queriesObj.GetOrdersToList();
            //System.out.println("done refreshing list of orders");
            }catch(SQLException e){System.out.println("Failed getting list of orders from database.");}
        }
      @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Tab = new javax.swing.JTabbedPane();
        mainBossPanel1 = new UserInterface.Panels.MainBossPanel();
        mainDeliveryManPanel2 = new UserInterface.Panels.MainDeliveryManPanel();
        mainWaitressPanel2 = new UserInterface.Panels.WaitressPanels.MainWaitressPanel();
        mainCookPanel2 = new UserInterface.Panels.MainCookPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout mainBossPanel1Layout = new javax.swing.GroupLayout(mainBossPanel1);
        mainBossPanel1.setLayout(mainBossPanel1Layout);
        mainBossPanel1Layout.setHorizontalGroup(
            mainBossPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        mainBossPanel1Layout.setVerticalGroup(
            mainBossPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        Tab.addTab("Panel Lidera", mainBossPanel1);
        Tab.addTab("Panel Dostawcy", mainDeliveryManPanel2);

        mainWaitressPanel2.setMinimumSize(new java.awt.Dimension(700, 700));
        mainWaitressPanel2.setPreferredSize(new java.awt.Dimension(905, 728));
        Tab.addTab("Panel Kelnerki", mainWaitressPanel2);
        mainWaitressPanel2.getAccessibleContext().setAccessibleParent(this);

        Tab.addTab("Panel Kucharza", mainCookPanel2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Tab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Tab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTabbedPane Tab;
    private UserInterface.Panels.MainBossPanel mainBossPanel1;
    private UserInterface.Panels.MainCookPanel mainCookPanel2;
    private UserInterface.Panels.MainDeliveryManPanel mainDeliveryManPanel2;
    private UserInterface.Panels.WaitressPanels.MainWaitressPanel mainWaitressPanel2;
    // End of variables declaration//GEN-END:variables
}


// Extra class extends TimerTask
// task for every minute
/*class MyTimerTask extends TimerTask
 {
  @Override
  public void run()
  {
      try{
          MainFrame.OrderList = MainFrame.queriesObj.GetOrdersToList();
          System.out.println("done refreshing list of orders");
         //MainFrame.OrderList = Queries.getOrdersToList();
      }catch(Exception e){System.out.println("automatic refreshing of orders failed!");e.printStackTrace();}
  }
 }*/