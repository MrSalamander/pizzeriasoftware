package Controller;

public class Meal{
    public int Id;
    public String Name;
    public float Price[];
    public String Ingredients;

    public Meal(int Id, String Name, String Ingredients, float[] Price)
    {
        this.Id = Id;
        this.Name = Name;
        this.Ingredients = Ingredients;
        this.Price = Price;
    }

}
