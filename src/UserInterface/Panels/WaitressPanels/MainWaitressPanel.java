package UserInterface.Panels.WaitressPanels;

import UserInterface.MainFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainWaitressPanel extends JPanel implements ActionListener {
    MainFrame mainFrame;
    protected GridLayout layout = new GridLayout(0,2);
    final public CardLayout cardLayout = new CardLayout();
    final public JPanel cards = new JPanel(cardLayout);
    JButton remoteOrder;
    JButton atLocationOrder;
    JPanel mainPanel;
    RemoteOrderPanel remoteOrderPanel;
    atLocationOrderPanel atLocationOrderPanel;
    public MainWaitressPanel(MainFrame mainFrame)
    {
        super();
        this.mainFrame = mainFrame;
        this.setLayout(cardLayout);
        this.setBackground(new Color(5,5,5,5));
        // 2 Buttons (Remote | atLocation)
        remoteOrder = new JButton("Dodaj Zamówienie na Wynos!");
        remoteOrder.addActionListener(this);
        remoteOrder.setFont(new Font("Arial", Font.PLAIN, 40));
        atLocationOrder = new JButton("Dodaj Zamówienie na Miejscu!");
        atLocationOrder.addActionListener(this);
        atLocationOrder.setFont(new Font("Arial", Font.PLAIN, 40));

        mainPanel = new JPanel();
        mainPanel.setLayout(layout);
        mainPanel.add(remoteOrder);
        mainPanel.add(atLocationOrder);

        atLocationOrderPanel = new atLocationOrderPanel(this);
        remoteOrderPanel = new RemoteOrderPanel(this);
        add("RemoteOrderPanel",remoteOrderPanel);
        add("ChooseOrder",mainPanel);
        add("atLocationOrderPanel",atLocationOrderPanel);

        cardLayout.show(this,"ChooseOrder");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if(source == remoteOrder)
        {
            cardLayout.show(this,"RemoteOrderPanel");
        }
        if(source == atLocationOrderPanel){

        }
    }
}
