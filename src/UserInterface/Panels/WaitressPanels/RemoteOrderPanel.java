package UserInterface.Panels.WaitressPanels;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RemoteOrderPanel extends JPanel implements ActionListener {
    MainWaitressPanel mainWaitressPanel;
    JButton CancelOrder;
    JButton SendOrder;
    JTextField addressField;
    JTextField telField;
    JTextArea commentField;
    JLabel addressLabel;
    JLabel telLabel;
    JLabel commentLabel;

    JPanel dataPanel,panel2,panel3,bottomPanel;
    DefaultTableModel dtm = new DefaultTableModel(0,0);
    String header[] = {"Danie", "Rozmiar", "Koszt","Usuń"};
    public RemoteOrderPanel(MainWaitressPanel mainWaitressPanel)
    {
        super();


        GroupLayout groupLayout = new GroupLayout(this);
        groupLayout.setAutoCreateGaps(true);
        this.setLayout(groupLayout);
        groupLayout.setAutoCreateContainerGaps(true);
        this.setBackground(new Color(255, 12, 39,50));

        JPanel addressPanel,telPanel,commentPanel;


        dataPanel = new JPanel();
        addressPanel = new JPanel();
        addressLabel = new JLabel("Adres: ");
        addressField = new JTextField("");
        addressField.setPreferredSize(new Dimension(200,40));
        addressPanel.add(addressLabel);
        addressPanel.add(addressField);
        dataPanel.add(addressPanel);

        telPanel = new JPanel(new FlowLayout());
        telLabel = new JLabel("Telefon: ");
        telField = new JTextField("");
        telField.setPreferredSize(new Dimension(200,40));
        telPanel.add(telLabel);
        telPanel.add(telField);
        dataPanel.add(telPanel);

        commentPanel = new JPanel(new FlowLayout());
        commentLabel = new JLabel("Komentarz: ");
        commentField = new JTextArea("");
        commentField.setLineWrap(true);
        commentField.setPreferredSize(new Dimension(200,60));
        commentPanel.add(commentLabel);
        commentPanel.add(commentField);
        dataPanel.add(commentPanel);
        this.add(dataPanel);



        // 2 Buttons (accept and cancel)
        bottomPanel = new JPanel(new GridLayout(0,2,10,20));
        this.mainWaitressPanel = mainWaitressPanel;
        CancelOrder = new JButton("Anuluj zamówienie");
        CancelOrder.addActionListener(this);
        bottomPanel.add(CancelOrder);

        SendOrder = new JButton("Złóż zamówienie");
        SendOrder.addActionListener(this);
        bottomPanel.add(SendOrder);
        this.add(bottomPanel);
        bottomPanel.setAlignmentY(Component.BOTTOM_ALIGNMENT);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if(source == CancelOrder)
        {
            mainWaitressPanel.cardLayout.show(mainWaitressPanel,"ChooseOrder");
        }

    }
}

