package UserInterface.Panels;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import UserInterface.*;
import Controller.Queries;

public class LoginPanel extends JPanel implements ActionListener {

    JTextField loginField;
    JPasswordField passwordField;
    JLabel loginLabel,passwordLabel;
    JButton resetButton,acceptButton;
    MainFrame mainFrame;
    public LoginPanel(MainFrame mainFrame)
    {
        super();
        this.mainFrame = mainFrame;
        mainFrame.setSize(400,250);
        FlowLayout flowLayout = new FlowLayout(FlowLayout.CENTER);
        flowLayout.setHgap(20);
        GridLayout layout = new GridLayout(0, 1);
        this.setLayout(layout);

        JPanel login, password,buttons;
        login = new JPanel(flowLayout);
        password = new JPanel(flowLayout);
        buttons = new JPanel(flowLayout);

        // Flow Login panel
        loginLabel = new JLabel("Login");
        loginField = new JTextField(20);
        login.add(loginLabel);
        login.add(loginField);
        this.add(login);

        // Flow Password panel
        passwordLabel = new JLabel("Hasło");
        passwordField = new JPasswordField(20);
        password.add(passwordLabel);
        password.add(passwordField);
        this.add(password);

        // Flow Buttons panel
        resetButton = new JButton("Resetuj");
        acceptButton = new JButton("Zaloguj");
        resetButton.addActionListener(this);
        acceptButton.addActionListener(this);
        buttons.add(acceptButton);
        buttons.add(resetButton);
        this.add(buttons);

        setBackground(Color.LIGHT_GRAY);
    }
    private void LoginCheckout(int status){

        switch (status)
        {
            case 0:
                JOptionPane.showMessageDialog(mainFrame.frame, "podano zły login bądź hasło!");
                break;
            case 1:
                mainFrame.LoginStatus = 1;
                JOptionPane.showMessageDialog(mainFrame.frame, "Zalogowano jako: Lider");
                mainFrame.spawnPanels();
                break;
            case 2:
                mainFrame.LoginStatus = 2;
                JOptionPane.showMessageDialog(mainFrame.frame, "Zalogowano jako: Kucharz");
                mainFrame.spawnPanels();
                break;
            case 3:
                mainFrame.LoginStatus = 3;
                JOptionPane.showMessageDialog(mainFrame.frame, "Zalogowano jako: Kelner");
                mainFrame.spawnPanels();
                break;
            case 4:
                mainFrame.LoginStatus = 4;
                JOptionPane.showMessageDialog(mainFrame.frame, "Zalogowano jako: Dostawca");
                mainFrame.spawnPanels();
                break;
            default:
                JOptionPane.showMessageDialog(mainFrame.frame, "coś poszło nie tak");
                break;
        }

    }
    @Override
    public void actionPerformed(ActionEvent e) {
       Object source = e.getSource();
       if(source == resetButton)
       {
           loginField.setText("");
           passwordField.setText("");
       }
       if(source == acceptButton)
       {
           String login = loginField.getText();
           String password = passwordField.getText();
           int loginStatus = Queries.login(login,password);
           LoginCheckout(loginStatus);
       }



    }


}
