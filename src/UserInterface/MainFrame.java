package UserInterface;


import UserInterface.Panels.*;
import UserInterface.Panels.WaitressPanels.MainWaitressPanel;

import javax.swing.*;

import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

public class MainFrame{
    public JFrame frame = new JFrame("Pizzeria FedikoSollini & Kamińskonacci");
    JTabbedPane tabs = new JTabbedPane();
    public int LoginStatus = 0;
    LoginPanel loginPanel;
    MainBossPanel mainBossPanel;
    MainCookPanel mainCookPanel;
    MainDeliveryManPanel mainDeliveryManPanel;
    MainWaitressPanel mainWaitressPanel;

    public static void main(String[] args) {
        MainFrame mainFrame = new MainFrame();
    }

    public MainFrame(){
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        initComponetns();
        frame.setVisible(true);
        frame.setDefaultLookAndFeelDecorated(true);
        //cardLayout.show(cards,"LoginPanel");
    }
    // inicjuje startowy panel logowania:
    private void initComponetns() {
        loginPanel = new LoginPanel(this);
        tabs.add("Panel Logowania", loginPanel);
        frame.add(tabs);
        //cards.add("MainBossPanel",new MainBossPanel(this));
        //cards.add("LoginPanel",loginPanel = new LoginPanel(this));
    }

    public void setSize(int width, int height)
    {
        this.frame.setSize(width,height);
    }

    public void spawnPanels(){
        setSize(1300,800);
        if(LoginStatus == 1){
            mainBossPanel = new MainBossPanel(this);
            tabs.add("Panel Lidera",mainBossPanel);
            mainCookPanel = new MainCookPanel(this);
            tabs.add("Panel Kucharza", mainCookPanel);
            mainWaitressPanel = new MainWaitressPanel(this);
            tabs.add("Panel Kelnerki/CC",mainWaitressPanel);
            mainDeliveryManPanel = new MainDeliveryManPanel(this);
            tabs.add("Panel Dostawcy", mainDeliveryManPanel);
        }else
        frame.remove(tabs);

        if(LoginStatus == 2) {
            mainCookPanel = new MainCookPanel(this);
            frame.add(mainCookPanel);
        }
        if(LoginStatus == 3) {
            mainWaitressPanel = new MainWaitressPanel(this);
            frame.add(mainWaitressPanel);
        }
        if(LoginStatus == 4){
            mainDeliveryManPanel = new MainDeliveryManPanel(this);
            frame.add(mainDeliveryManPanel);
        }
        tabs.remove(loginPanel);

    }

}
